package com.codingforcookies.alex.bootstrap.module.loader;

import java.util.regex.Pattern;

import com.codingforcookies.alex.api.IModule;
import com.codingforcookies.alex.bootstrap.module.ModuleInternal;

public interface IModuleLoader {
    public Pattern[] getPluginFileFilters();
    public IModule loadModule(ModuleInternal module);
    public void unloadModule(String moduleId);
}