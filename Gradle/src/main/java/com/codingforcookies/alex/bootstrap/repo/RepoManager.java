package com.codingforcookies.alex.bootstrap.repo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.codingforcookies.alex.bootstrap.AlexBootstrap;
import com.codingforcookies.alex.launch.Alex;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

public class RepoManager {
	private static final HashMap<String, Class<?>> repoAPIs = new HashMap<String, Class<?>>();
	
	static {
		repoAPIs.put("bitbucket.org", RepoAPIBitBucket.class);
	}
	
	private final Gson gson = new Gson();
	
	private AlexBootstrap bootstrap;
	private File reposFile;
	
	private List<ModuleRepo> repos = new ArrayList<ModuleRepo>();
	public List<ModuleRepo> getRepos() { return repos; }
	public ModuleRepo getModuleRepo(String id) {
		for(ModuleRepo repo : repos)
			if(repo.getRepoID().equals(id))
				return repo;
		return null;
	}
	
	public RepoManager(AlexBootstrap bootstrap) {
		this.bootstrap = bootstrap;
		reposFile = new File(bootstrap.getAlexFolder(), "repos");
		
		if(!reposFile.exists()) {
			try {
				reposFile.createNewFile();
			} catch (IOException e) { }
			
			repos.add(new ModuleRepo("alex-modules", "https://anonymous@bitbucket.org/codingforcookies/alex-modules.git"));
			
			saveRepos();
		}
	}
	
	public void saveRepos() {
		try(FileWriter writer = new FileWriter(reposFile)) {
			writer.write(gson.toJson(repos));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void loadRepos() throws FileNotFoundException {
		bootstrap.loadingOperation.setMainOperation("Loading repositories...");
		repos.clear();
		
		JsonReader reader = new JsonReader(new FileReader(reposFile));
		ModuleRepo[] loadedRepos = new Gson().fromJson(reader, ModuleRepo[].class);
	    for(ModuleRepo repo : loadedRepos)
	    	repos.add(repo);
	}

	public void buildRepoInformation() {
		bootstrap.loadingOperation.setSubOperation("Building repo information...");
		
		for(ModuleRepo repo : repos) {
			IRepoAPI api = null;
			
			for(Entry<String, Class<?>> entry : repoAPIs.entrySet()) {
				if(repo.getRepoURL().contains(entry.getKey())) {
					try {
						api = (IRepoAPI)entry.getValue().getDeclaredConstructor(String.class).newInstance(repo.getRepoURL());
					} catch (Exception e) { e.printStackTrace(); }
					break;
				}
			}
			
			if(api == null) {
				Alex.logger.error("Unknown repo type! Offender: " + repo.getRepoURL());
				continue;
			}
			
			repo.repoAPI = api;
		}
	}
}