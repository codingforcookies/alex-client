package com.codingforcookies.alex.bootstrap.proxy;

public interface IProxy {
	void init();
}