package com.codingforcookies.alex.bootstrap.module;

public class ModuleException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public ModuleException(String error) {
		super(error);
	}
}