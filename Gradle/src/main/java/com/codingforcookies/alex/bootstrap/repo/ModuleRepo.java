package com.codingforcookies.alex.bootstrap.repo;

public class ModuleRepo {
	private String repoID = "";
	public String getRepoID() { return repoID; }
	
	private String repoUrl = "";
	public String getRepoURL() { return repoUrl; }
	
	protected IRepoAPI repoAPI;
	public IRepoAPI getAPI() { return repoAPI; }
	
	public ModuleRepo() { }
	
	public ModuleRepo(String id, String url) {
		this.repoID = id;
		this.repoUrl = url;
	}
}