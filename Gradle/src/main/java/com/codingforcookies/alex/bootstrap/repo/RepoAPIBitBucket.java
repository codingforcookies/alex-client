package com.codingforcookies.alex.bootstrap.repo;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Base64;
import java.util.HashMap;

import com.codingforcookies.alex.api.util.AlexUtil;
import com.codingforcookies.alex.bootstrap.module.ModuleInternal;
import com.google.gson.Gson;

public class RepoAPIBitBucket implements IRepoAPI {
	private String url = "";
	
	boolean loaded = false;
	private HashMap<String, ModuleRepoInformation> moduleInformation = new HashMap<String, ModuleRepoInformation>();
	
	public RepoAPIBitBucket(String url) {
		String[] parts = url.split("bitbucket\\.org/", 2)[1].split("/");
		String org = parts[0];
		String repo = parts[1].split("\\.", 2)[0];
		
		this.url = "https://api.bitbucket.org/1.0/repositories/" + org + "/" + repo + "/";
	}
	
	@Override
	public String[] getModules() {
		if(!loaded)
			loadModules();
		return moduleInformation.keySet().toArray(new String[moduleInformation.size()]);
	}

	@Override
	public ModuleRepoInformation getModule(String module) {
		if(!loaded)
			loadModules();
		return moduleInformation.get(module);
	}
	
	private void loadModules() {
		String content;
		try {
			content = AlexUtil.readUrl(url + "src/master/");
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		
		Gson gson = new Gson();
		GitDirectory gitdir = gson.fromJson(content, GitDirectory.class);
		
		for(String dir : gitdir.directories) {
			String dircontent;
			try {
				dircontent = AlexUtil.readUrl(url + "src/master/" + dir + "/");
			} catch (Exception e) {
				e.printStackTrace();
				continue;
			}
			
			GitDirectory gitver = gson.fromJson(dircontent, GitDirectory.class);
			moduleInformation.put(dir, new ModuleRepoInformation(dir, gitver.directories));
		}
	}

	@Override
	public void update(ModuleInternal module) throws Exception {
		ModuleRepoInformation info = getModule(module.info.id);
		String content;
		try {
			content = AlexUtil.readUrl(url + "src/master/" + info.id + "/" + info.getLatestVersion().version + "/" + module.info.name + ".jar");
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		
		Gson gson = new Gson();
		GitFile gitfile = gson.fromJson(content, GitFile.class);
		
		if(module.moduleFile.exists())
			module.moduleFile.delete();
		module.moduleFile.createNewFile();
		
		byte[] data = Base64.getDecoder().decode(gitfile.data);
		try(OutputStream stream = new FileOutputStream(module.moduleFile)) {
		    stream.write(data);
		}
	}
}

class GitDirectory {
	String node;
	String path;
	String[] directories;
	String[] files;
}

class GitFile {
	String node;
	String path;
	int size;
	String data;
	String encoding;
}