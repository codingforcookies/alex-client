package com.codingforcookies.alex.bootstrap.proxy;

import org.lwjgl.opengl.GL11;

import com.codingforcookies.alex.api.AlexAPI;
import com.codingforcookies.alex.api.event.EventRender;
import com.codingforcookies.alex.api.event.EventRenderPlayer;
import com.codingforcookies.alex.api.event.EventRenderWorld;
import com.codingforcookies.alex.api.event.EventUpdate;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderPlayerEvent;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

public class ProxyForge implements IProxy {
	public void init() {
		MinecraftForge.EVENT_BUS.register(this);
	}

	@SubscribeEvent
	public void onClientTick(TickEvent.ClientTickEvent event) {
		if(event.phase == TickEvent.Phase.START)
			AlexAPI.EVENT_BUS.EVENTS.fireEvent(EventUpdate.class);
	}

	@SubscribeEvent(priority = EventPriority.HIGHEST)
	public void onClientRender(RenderGameOverlayEvent.Post event) {
		AlexAPI.EVENT_BUS.EVENTS.fireEvent(EventRender.class);
	}
	
	@SubscribeEvent(priority = EventPriority.HIGHEST)
	public void onRenderWorld(RenderWorldLastEvent event) {
		popCoords(event.getPartialTicks());
		AlexAPI.EVENT_BUS.EVENTS.fireEvent(EventRenderWorld.class);
	}

	@SubscribeEvent
	public void onPlayerRender(RenderPlayerEvent.Post event) {
		AlexAPI.EVENT_BUS.EVENTS.fireEvent(EventRenderPlayer.class);
	}

	private void popCoords(float partialTicks) {
		EntityPlayer p = Minecraft.getMinecraft().thePlayer;
		double playerX = p.lastTickPosX + (p.posX - p.lastTickPosX) * partialTicks;
		double playerY = p.lastTickPosY + (p.posY - p.lastTickPosY) * partialTicks;
		double playerZ = p.lastTickPosZ + (p.posZ - p.lastTickPosZ) * partialTicks;

		GL11.glTranslated(-playerX, -playerY, -playerZ);
	}
}