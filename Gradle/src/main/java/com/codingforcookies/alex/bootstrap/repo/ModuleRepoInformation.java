package com.codingforcookies.alex.bootstrap.repo;

public class ModuleRepoInformation {
	String id;
	GitVersion[] versions;
	
	public ModuleRepoInformation(String id, String[] versions) {
		this.id = id;
		this.versions = new GitVersion[versions.length];
		
		for(int i = 0; i < versions.length; i++)
			this.versions[i] = new GitVersion(versions[i]);
	}
	
	public GitVersion[] getVersions() {
		return versions;
	}
	
	public GitVersion getLatestVersion() {
		GitVersion latest = versions[0];
		for(GitVersion v : versions)
			if(latest.isNewer(v))
				latest = v;
		return latest;
	}
}