package com.codingforcookies.alex.bootstrap;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;

import com.codingforcookies.alex.api.AlexAPI;
import com.codingforcookies.alex.api.IModule;
import com.codingforcookies.alex.api.InputHelper;
import com.codingforcookies.alex.bootstrap.module.ModuleLoadHandler;
import com.codingforcookies.alex.bootstrap.module.loader.ModuleClassLoader;
import com.codingforcookies.alex.bootstrap.proxy.IProxy;
import com.codingforcookies.alex.bootstrap.proxy.ProxyForge;
import com.codingforcookies.alex.bootstrap.repo.RepoManager;
import com.codingforcookies.alex.launch.Alex;

public class AlexBootstrap {
	private static final String VERSION = "${version}";
	
	private File alexFolder;
	public File getAlexFolder() { return alexFolder; }
	
	private RepoManager repoManager;
	public RepoManager getRepoManager() { return repoManager; }
	
	private ModuleLoadHandler moduleLoader;
	public ModuleLoadHandler getModuleLoader() { return moduleLoader; }
	
	public HashMap<String, IModule> modules = new HashMap<String, IModule>();
	public IModule getInstance(String moduleName) {
		if(modules.containsKey(moduleName))
			return modules.get(moduleName);
		return null;
	}
	
	public BootstrapLoadingOperation loadingOperation;
	
	public final IProxy proxy;
	
	public AlexBootstrap(File mcFolder) {
		Alex.logger.info("Alex Client Bootstrap v." + VERSION);
		
		alexFolder = new File(mcFolder, "AlexClient");
		if(!alexFolder.exists())
			alexFolder.mkdirs();
		
		loadingOperation = new BootstrapLoadingOperation();
		loadingOperation.setOverallOperation("Welcome.");
		loadingOperation.setMainOperation("Alex is verifying your installation.");
		loadingOperation.setSubOperation("This will only take a moment.");
		
		repoManager = new RepoManager(this);
		moduleLoader = new ModuleLoadHandler(this, repoManager);
	
		proxy = new ProxyForge();
		proxy.init();

		InputHelper ih = new InputHelper();
		AlexAPI.EVENT_BUS.register(null, ih);
	}
	
	/**
	 * Load stored repos, build repo information, get all modules in the Modules folder, verify them and update
	 * if needed. Finally, load them into memory.
	 */
	public void beginLoading() {
		new Thread(() -> {
			try {
				Thread.sleep(5000L);
				
				loadingOperation.setOverallOperation("Initializing bootstrap...");

				repoManager.loadRepos();
				Thread.sleep(500L);
				
				repoManager.buildRepoInformation();
				Thread.sleep(500L);

				moduleLoader.detectModules();
				Thread.sleep(500L);

				moduleLoader.verifyModules();
				Thread.sleep(500L);

				loadingOperation.setOverallOperation("Loading modules...");
				moduleLoader.loadModules();
				Thread.sleep(500L);

				loadingOperation.setOverallOperation("Finalizing.");
				finishLoading();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				loadingOperation.setFailedOperation("Unable to load repositories.");
			} catch (Exception e) {
				e.printStackTrace();
				loadingOperation.setFailedOperation(e.getMessage());
			}
		}).start();
	}
	
	/**
	 * Resync with the main thread. Makes sure that OpenGL is availble if needed during load.
	 */
	public void finishLoading() {
		for(ModuleClassLoader loader : moduleLoader.getLoader().getClassLoaders())
			loader.module.onLoad();

		loadingOperation.setCompleted();
	}
}