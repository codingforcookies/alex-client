package com.codingforcookies.alex.bootstrap;

import com.codingforcookies.alex.launch.Alex;

public class BootstrapLoadingOperation {
	private boolean completed = false;
	public boolean isCompleted() { return completed; }
	
	private String overallOperation = "";
	public String getOverallOperation() { return overallOperation; }
	
	private String mainOperation = "";
	public String getMainOperation() { return mainOperation; }
	
	private String subOperation = "";
	public String getSubOperation() { return subOperation; }
	
	public void setOverallOperation(String overallOperation) {
		Alex.logger.info(overallOperation);
		this.overallOperation = overallOperation;
	}
	
	public void setMainOperation(String mainOperation) {
		Alex.logger.info(mainOperation);
		this.mainOperation = mainOperation;
		this.subOperation = "";
	}
	
	public void setSubOperation(String subOperation) {
		Alex.logger.info(subOperation);
		this.subOperation = subOperation;
	}
	
	public void setFailedOperation(String error) {
		setMainOperation("Critical error. Unable to continue.");
		setSubOperation(error);
	}

	public void setCompleted() {
		completed = true;
	}
}