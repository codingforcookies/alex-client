package com.codingforcookies.alex.bootstrap.module;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;

public class JarHelper {
	private static final int BUFFER_SIZE = 2156;
	private byte[] mBuffer = new byte[BUFFER_SIZE];
	private int mByteCount = 0;
	private boolean mVerbose = false;
	private String mDestJarName = "";
	
	public void jarDir(File dirOrFile2Jar, File destJar) throws IOException {
		if (dirOrFile2Jar == null || destJar == null)
			throw new IllegalArgumentException();

		mDestJarName = destJar.getCanonicalPath();
		FileOutputStream fout = new FileOutputStream(destJar);
		JarOutputStream jout = new JarOutputStream(fout);
		try {
			jarDir(dirOrFile2Jar, jout, null);
		} catch (IOException ioe) {
			throw ioe;
		} finally {
			jout.close();
			fout.close();
		}
	}
	
	private static final char SEP = '/';

	private void jarDir(File dirOrFile2jar, JarOutputStream jos, String path) throws IOException {
		if (mVerbose)
			System.out.println("checking " + dirOrFile2jar);
		if (dirOrFile2jar.isDirectory()) {
			String[] dirList = dirOrFile2jar.list();
			String subPath = (path == null) ? "" : (path + dirOrFile2jar.getName() + SEP);
			if (path != null) {
				JarEntry je = new JarEntry(subPath);
				je.setTime(dirOrFile2jar.lastModified());
				jos.putNextEntry(je);
				jos.flush();
				jos.closeEntry();
			}
			for (int i = 0; i < dirList.length; i++) {
				File f = new File(dirOrFile2jar, dirList[i]);
				jarDir(f, jos, subPath);
			}
		} else {
			if (dirOrFile2jar.getCanonicalPath().equals(mDestJarName)) {
				if (mVerbose)
					System.out.println("skipping " + dirOrFile2jar.getPath());
				return;
			}

			if (mVerbose)
				System.out.println("adding " + dirOrFile2jar.getPath());
			FileInputStream fis = new FileInputStream(dirOrFile2jar);
			try {
				JarEntry entry = new JarEntry(path + dirOrFile2jar.getName());
				entry.setTime(dirOrFile2jar.lastModified());
				jos.putNextEntry(entry);
				while ((mByteCount = fis.read(mBuffer)) != -1) {
					jos.write(mBuffer, 0, mByteCount);
					if (mVerbose)
						System.out.println("wrote " + mByteCount + " bytes");
				}
				jos.flush();
				jos.closeEntry();
			} catch (IOException ioe) {
				throw ioe;
			} finally {
				fis.close();
			}
		}
	}
}