package com.codingforcookies.alex.bootstrap.module.loader;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Pattern;

import com.codingforcookies.alex.api.IModule;
import com.codingforcookies.alex.bootstrap.module.ModuleInternal;

public class ModuleLoader implements IModuleLoader {
	private final Pattern[] fileFilters = new Pattern[] { Pattern.compile("\\.jar$"), };
	private final Map<String, Class<?>> classes = new HashMap<String, Class<?>>();
	private final Map<String, ModuleClassLoader> loaders = new LinkedHashMap<String, ModuleClassLoader>();
	public Collection<ModuleClassLoader> getClassLoaders() { return loaders.values(); }

	public Pattern[] getPluginFileFilters() {
		return fileFilters.clone();
	}

	Class<?> getClassByName(final String name) {
		Class<?> cachedClass = classes.get(name);

		if(cachedClass != null) {
			return cachedClass;
		}else{
			for(String current : loaders.keySet()) {
				ModuleClassLoader loader = loaders.get(current);

				try {
					cachedClass = loader.findClass(name, false);
				} catch (ClassNotFoundException cnfe) {}
				if (cachedClass != null) {
					return cachedClass;
				}
			}
		}
		return null;
	}

	void setClass(final String name, final Class<?> clazz) {
		if(!classes.containsKey(name))
			classes.put(name, clazz);
	}

	private void removeClass(String name) {
		classes.remove(name);
	}

	@Override
	public IModule loadModule(ModuleInternal module) {
		ModuleClassLoader loader = null;
		try {
			loader = new ModuleClassLoader(this, getClass().getClassLoader(), module);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		loaders.put(module.info.id, loader);
		
		return loader.module;
	}

	@Override
	public void unloadModule(String moduleId) {
		ModuleClassLoader jcl = loaders.get(moduleId);
		if(jcl == null)
			return;
		
		System.out.println("Unloading " + moduleId);
		
		jcl.module.onUnload();

		for(String name : jcl.getClasses())
			removeClass(name);

		loaders.remove(moduleId);
	}
}