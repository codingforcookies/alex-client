package com.codingforcookies.alex.bootstrap.module;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import com.codingforcookies.alex.api.IModule;
import com.codingforcookies.alex.api.util.AlexUtil;
import com.codingforcookies.alex.bootstrap.AlexBootstrap;
import com.codingforcookies.alex.bootstrap.module.loader.ModuleLoader;
import com.codingforcookies.alex.bootstrap.repo.GitVersion;
import com.codingforcookies.alex.bootstrap.repo.ModuleRepo;
import com.codingforcookies.alex.bootstrap.repo.ModuleRepoInformation;
import com.codingforcookies.alex.bootstrap.repo.RepoManager;
import com.codingforcookies.alex.launch.Alex;
import com.google.gson.Gson;

public class ModuleLoadHandler {
	private AlexBootstrap bootstrap;
	private RepoManager repoManager;
	
	private File moduleFolder;
	public File getModuleFolder() { return moduleFolder; }

	private ModuleDependencies dependencyTree = new ModuleDependencies();
	private ModuleLoader moduleLoader;
	public ModuleLoader getLoader() { return moduleLoader; }
	
	public ModuleLoadHandler(AlexBootstrap bootstrap, RepoManager repoManager) {
		this.bootstrap = bootstrap;
		this.repoManager = repoManager;
		
		this.moduleFolder = new File(bootstrap.getAlexFolder(), "Modules");
		
		if(!moduleFolder.exists() && !moduleFolder.mkdirs())
			Alex.logger.error("Failed to create " + moduleFolder.getAbsolutePath());
		
		moduleLoader = new ModuleLoader();
	}
	
	public void verifyModule(ModuleInternal module) throws ModuleException {
		bootstrap.loadingOperation.setSubOperation("Verifying " + module.info.name + "(" + module.info.id + ")...");
		
		ModuleRepo repo = repoManager.getModuleRepo(module.info.repo);
		if(repo == null)
			throw new ModuleException("Module repo not found.");

		ModuleRepoInformation modInfo = repo.getAPI().getModule(module.info.id);
		if(modInfo == null)
			throw new ModuleException("Module not found in repo.");
		
		if(module.gitVersion.isNewer(modInfo.getLatestVersion())) {
			bootstrap.loadingOperation.setMainOperation("Updating " + module.info.name + "(" + module.info.id + ")...");
			
			try {
				repo.getAPI().update(module);
			} catch (Exception e) {
				e.printStackTrace();
				bootstrap.loadingOperation.setSubOperation("Failed.");
			}
		}
	}
	
	public ModuleInternal readModule(File modFile) {
		Gson gson = new Gson();
		
		if(modFile.isDirectory()) {
			
		}else{
			try(ZipFile file = new ZipFile(modFile)) {
				if(file != null) {
					Enumeration<? extends ZipEntry> entries = file.entries();
	
					if(entries != null) {
						while(entries.hasMoreElements()) {
							ZipEntry entry = entries.nextElement();
							if(!entry.getName().endsWith(".alexmod"))
								continue;
							
							ModuleInternal mod = new ModuleInternal();
							mod.moduleFile = modFile;
							mod.info = gson.fromJson(AlexUtil.readStream(file.getInputStream(entry)).trim(), ModuleInfo.class);
							mod.gitVersion = new GitVersion(mod.info.version);
							
							return mod;
						}
	
						return null;
					}
				}
	
				file.close();
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		}
		
		return null;
	}

	public void detectModules() throws Exception {
		File[] possibleFiles = moduleFolder.listFiles();
		
		for(File possible : possibleFiles) {
			if(possible.isDirectory()) {
				JarHelper jar = new JarHelper();
				jar.jarDir(possible, new File(possible.getParentFile(), possible.getName() + ".jar"));
			}
		}
		
		possibleFiles = moduleFolder.listFiles();

		bootstrap.loadingOperation.setMainOperation("Detecting modules...");
		bootstrap.loadingOperation.setSubOperation("Reading " + possibleFiles.length + " possible module" + (possibleFiles.length != 1 ? "s" : "") + "...");

		for(File possible : possibleFiles) {
			ModuleInternal mod = readModule(possible);
			if(mod == null)
				continue;
			dependencyTree.addModule(mod);
		}
		
		if(!dependencyTree.modules.containsKey("alex-core")) {
			bootstrap.loadingOperation.setMainOperation("Downloading Alex Core...");
			
			ModuleRepo repo = repoManager.getModuleRepo("alex-modules");
			if(repo == null)
				throw new ModuleException("Missing main repo!");
			
			repo.getAPI().update(ModuleInternal.createDummy("alex-core", "Alex Core"));
		}
	}

	public void verifyModules() {
		bootstrap.loadingOperation.setMainOperation("Verifying modules...");
		
		for(ModuleInternal module : dependencyTree.modules.values()) {
			try {
				verifyModule(module);
			} catch (ModuleException e) { e.printStackTrace(); }
		}

		sortModules();
	}

	public void sortModules() {
		bootstrap.loadingOperation.setMainOperation("Sorting modules...");
		dependencyTree.doSort();
	}
	
	public void loadModules() {
		bootstrap.loadingOperation.setMainOperation("Loading " + dependencyTree.modules.size() + " modules...");

		for(String module : dependencyTree.modules.keySet())
			loadModule(module);
	}
	
	public void loadModule(String id) {
		bootstrap.loadingOperation.setSubOperation("Loading " + id + "...");
		
		ModuleInternal module = dependencyTree.modules.get(id);
		IModule mod = moduleLoader.loadModule(module);
		bootstrap.modules.put(module.info.id, mod);
	}
	
	public void unload() {
		for(String module : bootstrap.modules.keySet())
			moduleLoader.unloadModule(module);
		bootstrap.modules.clear();
	}
}