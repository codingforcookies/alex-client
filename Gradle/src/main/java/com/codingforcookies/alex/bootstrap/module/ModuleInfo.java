package com.codingforcookies.alex.bootstrap.module;

public class ModuleInfo {
	public String main;

	public String id;
	public String apiversion;
	public String repo;
	
	public String name;
	public String description;
	public String version;
	public String url;
	public String[] authorList;
	
	public String[] depends;
}