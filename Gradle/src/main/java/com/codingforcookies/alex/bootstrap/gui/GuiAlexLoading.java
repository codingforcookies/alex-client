package com.codingforcookies.alex.bootstrap.gui;

import com.codingforcookies.alex.bootstrap.BootstrapLoadingOperation;
import com.codingforcookies.alex.launch.Alex;

import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;

public class GuiAlexLoading extends GuiScreen {
    public void updateScreen() {
    	if(Alex.get().getBootstrap().loadingOperation.isCompleted()) {
    		mc.displayGuiScreen(new GuiMainMenu());
    		Alex.get().getBootstrap().loadingOperation = null;
    	}
    }
    
	@Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		super.drawScreen(mouseX, mouseY, partialTicks);
		
		this.updateScreen();
		
    	this.drawGradientRect(0, 0, width, height, 0xFFEEEEEE, 0xFFB9B9B9);
		
    	BootstrapLoadingOperation loadingOperation = Alex.get().getBootstrap().loadingOperation;
    	if(loadingOperation == null)
    		return;
    	
    	drawRect(width / 2 - 150, height / 2 - 50, width / 2 + 150, height / 2 + 50, 0x99000000);
    	
    	GlStateManager.scale(2F, 2F, 1F);
    	drawCenteredString(fontRendererObj, loadingOperation.getOverallOperation(), width / 4, height / 4 - 20, 0xFFFF33);

    	GlStateManager.scale(.5F, .5F, 1F);
    	drawCenteredString(fontRendererObj, loadingOperation.getMainOperation(), width / 2, height / 2, 0xFFFFFF);
    	
    	GlStateManager.scale(.5F, .5F, 1F);
    	drawCenteredString(fontRendererObj, loadingOperation.getSubOperation(), width, height + 60, 0xEEEE22);
    }
}