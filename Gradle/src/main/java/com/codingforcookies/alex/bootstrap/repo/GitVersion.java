package com.codingforcookies.alex.bootstrap.repo;

public class GitVersion {
	public String version = "";
	public int major = 0;
	public int minor = 0;
	public int build = 0;
	
	public GitVersion(String version) {
		this.version = version;
		
		if(!version.contains("."))
			return;
		
		String[] bits = version.split("\\.");
		major = Integer.valueOf(bits[0]);
		minor = Integer.valueOf(bits[1]);
		build = Integer.valueOf(bits[2]);
	}
	
	public boolean isNewer(GitVersion version) {
		return (version.major > major || version.major > minor || version.build > build);
	}
}