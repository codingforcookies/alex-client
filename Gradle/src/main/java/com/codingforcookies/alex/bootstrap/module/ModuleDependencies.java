package com.codingforcookies.alex.bootstrap.module;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class ModuleDependencies {
	public LinkedHashMap<String, ModuleInternal> modules = new LinkedHashMap<String, ModuleInternal>();
	private List<ModuleInternal> modulesUnsorted = new ArrayList<ModuleInternal>();

	public void addModule(ModuleInternal mod) {
		if(mod.info.depends == null)
			mod.info.depends = new String[0];
		
		if(!mod.info.id.equals("alex-core")) {
			String[] newDepends = new String[mod.info.depends.length + 1];
			newDepends[0] = "alex-core";
			for(int i = 1; i < newDepends.length; i++)
				newDepends[i] = mod.info.depends[i - 1];
			mod.info.depends = newDepends;
			
			modulesUnsorted.add(mod);
		}else
			modules.put(mod.info.id, mod);
	}
	
	public void doSort() {
		while(modulesUnsorted.size() > 0) {
			for(int i = 0; i < modulesUnsorted.size(); i++) {
				ModuleInternal mod = modulesUnsorted.get(i);
				
				// If all depends are in the sorted list
				boolean hasAllDepends = true;
				
				// If all depends are in the sorted list, or in the unsorted list
				boolean hasAllDependsLoaded = true;
				
				for(String depend : mod.info.depends) {
					if(!modules.containsKey(depend)) {
						hasAllDependsLoaded = false;
						
						boolean hasDepend = false;
						for(ModuleInternal modCheck : modulesUnsorted) {
							if(modCheck.info.id.equals(depend)) {
								hasDepend = true;
								break;
							}
						}
						
						if(!hasDepend) {
							hasAllDepends = false;
							break;
						}
					}
				}
				
				if(!hasAllDepends) {
					modulesUnsorted.remove(i);
					i--;
				}else if(hasAllDependsLoaded) {
					modules.put(mod.info.id, mod);
					modulesUnsorted.remove(i);
					i--;
				}
			}
		}
	}
}