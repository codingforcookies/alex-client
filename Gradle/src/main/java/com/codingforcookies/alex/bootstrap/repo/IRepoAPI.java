package com.codingforcookies.alex.bootstrap.repo;

import com.codingforcookies.alex.bootstrap.module.ModuleInternal;

public interface IRepoAPI {
	String[] getModules();
	ModuleRepoInformation getModule(String module);
	
	void update(ModuleInternal module) throws Exception;
}