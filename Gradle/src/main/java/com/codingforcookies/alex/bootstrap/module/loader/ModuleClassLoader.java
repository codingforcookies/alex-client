package com.codingforcookies.alex.bootstrap.module.loader;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.codingforcookies.alex.api.IModule;
import com.codingforcookies.alex.bootstrap.module.ModuleInternal;
import com.codingforcookies.alex.launch.Alex;

public class ModuleClassLoader extends URLClassLoader {
    private final ModuleLoader loader;
    private final Map<String, Class<?>> classes = new HashMap<String, Class<?>>();
    public final IModule module;

    ModuleClassLoader(ModuleLoader loader, ClassLoader parent, ModuleInternal module) throws MalformedURLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        super(new URL[] { module.moduleFile.toURI().toURL() }, parent);
        this.loader = loader;
        
        Class<?> jarClass = Class.forName(module.info.main, true, this);
        this.module = (IModule)jarClass.newInstance();
		
		Alex.logger.info("Initializing " + module.moduleFile.getName() + ": " + module.info.name + "(" + module.info.id + ")");
    }

	@Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        return findClass(name, true);
    }

    Class<?> findClass(String name, boolean checkGlobal) throws ClassNotFoundException {
        Class<?> result = classes.get(name);

        if(result == null) {
            if(checkGlobal)
                result = loader.getClassByName(name);

            if(result == null) {
                result = super.findClass(name);

                if(result != null)
                    loader.setClass(name, result);
            }

            classes.put(name, result);
        }

        return result;
    }

    Set<String> getClasses() {
        return classes.keySet();
    }
}