package com.codingforcookies.alex.bootstrap.module;

import java.io.File;

import com.codingforcookies.alex.bootstrap.repo.GitVersion;
import com.codingforcookies.alex.launch.Alex;

public class ModuleInternal {
	public static ModuleInternal createDummy(String id, String name) {
		ModuleInternal dummy = new ModuleInternal();
		dummy.moduleFile = new File(Alex.get().getBootstrap().getModuleLoader().getModuleFolder(), name + ".jar");
		
		dummy.info = new ModuleInfo();
		dummy.info.id = id;
		dummy.info.name = name;
		
		dummy.gitVersion = new GitVersion("0.0.0");
		
		return dummy;
	}
	
	public File moduleFile;
	
	public ModuleInfo info;
	
	public GitVersion gitVersion;
}