package com.codingforcookies.alex.api.event.handle;

import java.lang.reflect.Method;
import java.util.HashMap;

import com.codingforcookies.alex.api.IModule;
import com.codingforcookies.alex.api.event.base.AlexSubscribe;
import com.codingforcookies.alex.api.event.base.EventBase;
import com.codingforcookies.alex.api.event.base.EventReceivers;
import com.codingforcookies.alex.launch.Alex;

public class EventHandler {
	private HashMap<Class<? extends EventBase>, EventReceivers> receivers = new HashMap<Class<? extends EventBase>, EventReceivers>();
	
	@SuppressWarnings("unchecked")
	protected void register(IModule module, Object instance) {
		for(Method method : instance.getClass().getMethods()) {
			if(method.isAnnotationPresent(AlexSubscribe.class)) {
				Class<? extends EventBase> paramType = (Class<? extends EventBase>)method.getParameterTypes()[0];

				try {
					if(!receivers.containsKey(paramType))
						receivers.put(paramType, new EventReceivers(paramType.newInstance()));

					receivers.get(paramType).addReceiver(module, instance, method);
				} catch (IllegalAccessException e) {
					Alex.logger.error("Failure to register event. Offender: " + method.getName());
				} catch (InstantiationException e) {
					Alex.logger.error("Event must have a no-args constructor. Offender: " + paramType.getName());
				}
			}
		}
	}
	
	public void fireEvent(Class<?> eventClass) {
		if(!receivers.containsKey(eventClass))
			return;
		receivers.get(eventClass).execute();
	}
}