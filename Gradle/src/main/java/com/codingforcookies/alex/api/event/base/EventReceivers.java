package com.codingforcookies.alex.api.event.base;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.codingforcookies.alex.api.IModule;

public class EventReceivers {
	private class EventReceiver {
		protected IModule module;
		
		private Object instance;
		private MethodHandle method;
		
		public EventReceiver(IModule module, Object instance, Method method) throws IllegalAccessException {
			this.module = module;
			this.instance = instance;
			this.method = LOOKUP.unreflect(method);
		}
		
		public void execute(EventBase event) throws Throwable {
			method.invoke(instance, event);
		}
	}
	
	private static final MethodHandles.Lookup LOOKUP = MethodHandles.lookup();
	
	private EventBase event;
	private EventReceiver[] receivers = new EventReceiver[0];
	
	public EventReceivers(EventBase event) {
		this.event = event;
	}

	public void addReceiver(IModule module, Object instance, Method method) throws IllegalAccessException {
		List<EventReceiver> receivers = new ArrayList<>();
		receivers.addAll(Arrays.asList(this.receivers));
		receivers.add(new EventReceiver(module, instance, method));
		this.receivers = receivers.toArray(new EventReceiver[receivers.size()]);
	}
	
	public void unloadModule(IModule module) {
		List<EventReceiver> receivers = Arrays.asList(this.receivers);
		
		for(int i = 0; i < receivers.size(); i++) {
			if(receivers.get(i).module == module) {
				receivers.remove(i);
				i--;
			}
		}
		
		this.receivers = receivers.toArray(new EventReceiver[receivers.size()]);
	}

	public void execute() {
		for(EventReceiver receiver : receivers)
			try {
				receiver.execute(event);
			} catch (Throwable e) { e.printStackTrace(); }
	}
}