package com.codingforcookies.alex.api.event;

import com.codingforcookies.alex.api.event.base.EventAbstract;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;

public class EventRender extends EventAbstract {
	public String getName() {
		return "EventRender";
	}
	
	public FontRenderer fontRenderer;
	
	public EventRender() {
		fontRenderer = Minecraft.getMinecraft().fontRendererObj;
	}
}