package com.codingforcookies.alex.api.event.handle;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import com.codingforcookies.alex.api.IModule;
import com.codingforcookies.alex.api.event.key.KeyEventState;
import com.codingforcookies.alex.api.event.key.KeyListener;

/**
 * Terrible class. Remake this as soon as you have the time.
 * 
 * If this is still in at first release, Imma slap a bitch.
 */
public class KeyHandler {
	private HashMap<Integer, Method[][]> keyListeners = new HashMap<Integer, Method[][]>();
	
	protected void register(IModule module, Object instance) {
		try {
			for(Method method : instance.getClass().getMethods()) {
				if(method.isAnnotationPresent(KeyListener.class)) {
					KeyListener keyListener = method.getAnnotation(KeyListener.class);
	
					ArrayList<Method> methods = new ArrayList<Method>();
					
					if(keyListeners.containsKey(keyListener.key()))
						for(Method premethods : keyListeners.get(keyListener.key())[keyListener.keystate().ordinal()])
							methods.add(premethods);
					
					methods.add(method);
					
					Method[][] methodList;
					if(keyListeners.containsKey(keyListener.key()))
						methodList = keyListeners.get(keyListener.key());
					else
						methodList = new Method[KeyEventState.values().length - 1][0];
					methodList[keyListener.keystate().ordinal()] = methods.toArray(new Method[methods.size()]);
					
					keyListeners.put(keyListener.key(), methodList);
				}
			}
		} catch (Exception e) { }
	}
	
	public void fireEvent(int key, KeyEventState kes) {
		try {
			if(keyListeners.containsKey(key)) {
				for(Method method : keyListeners.get(key)[kes.ordinal()])
					try {
						method.invoke(null);
					} catch (Throwable e) { e.printStackTrace(); }
			}
		} catch(Exception e) { }
	}
}