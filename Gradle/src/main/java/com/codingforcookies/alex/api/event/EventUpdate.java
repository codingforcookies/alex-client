package com.codingforcookies.alex.api.event;

import com.codingforcookies.alex.api.event.base.EventAbstract;

public class EventUpdate extends EventAbstract {
	public String getName() {
		return "EventUpdate";
	}
}