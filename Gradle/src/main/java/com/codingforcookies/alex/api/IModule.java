package com.codingforcookies.alex.api;

public interface IModule {
	/**
	 * Runs when the module is loaded.
	 */
	public void onLoad();
	
	/**
	 * Runs when the module is unloaded.
	 */
	public void onUnload();
}