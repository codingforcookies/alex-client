package com.codingforcookies.alex.api.event.handle;

import com.codingforcookies.alex.api.IModule;

public class EventBus {
	public final EventHandler EVENTS = new EventHandler();
	public final KeyHandler KEY_EVENTS = new KeyHandler();

	public void register(IModule module, Object instance) {
		EVENTS.register(module, instance);
		KEY_EVENTS.register(module, instance);
	}
}