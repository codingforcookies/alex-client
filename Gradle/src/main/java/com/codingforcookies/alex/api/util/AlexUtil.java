package com.codingforcookies.alex.api.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;

import com.codingforcookies.alex.launch.Alex;

public class AlexUtil {
	public static void log(String message) {
		Alex.logger.info(message);
	}

	public static void error(String message) {
		Alex.logger.error(message);
	}

	public static File getAlexFolder() {
		return Alex.get().getBootstrap().getAlexFolder();
	}

	public static String exportResource(String resourceName, File location) throws Exception {
		String jarFolder;
		try(InputStream stream = AlexUtil.class.getResourceAsStream(resourceName)) {
			if(stream == null)
				throw new Exception("Cannot get resource \"" + resourceName + "\" from Jar file.");

			int readBytes;
			byte[] buffer = new byte[4096];
			jarFolder = new File(AlexUtil.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getParentFile().getPath().replace('\\', '/');
			
			try(OutputStream resStreamOut = new FileOutputStream(location)) {
				while ((readBytes = stream.read(buffer)) > 0)
					resStreamOut.write(buffer, 0, readBytes);
			} catch (Exception ex) {
				throw ex;
			}
		} catch (Exception ex) {
			throw ex;
		}

		return jarFolder + resourceName;
	}

	public static String readUrl(String urlString) throws Exception {
		BufferedReader reader = null;
		try {
			URL url = new URL(urlString);
			reader = new BufferedReader(new InputStreamReader(url.openStream()));
			StringBuffer buffer = new StringBuffer();
			int read;
			char[] chars = new char[1024];
			while((read = reader.read(chars)) != -1)
				buffer.append(chars, 0, read); 

			return buffer.toString();
		} finally {
			if(reader != null)
				reader.close();
		}
	}

	public static String readStream(java.io.InputStream inputStream) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while(line != null) {
				sb.append(line);
				sb.append("\n");
				line = br.readLine();
			}

			return sb.toString();
		} finally {
			br.close();
			inputStream.close();
		}
	}
}