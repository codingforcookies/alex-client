package com.codingforcookies.alex.api;

import com.codingforcookies.alex.api.event.handle.EventBus;
import com.codingforcookies.alex.bootstrap.repo.GitVersion;

import net.minecraft.client.gui.ScaledResolution;

public class AlexAPI {
	public static final GitVersion API_VERSION = new GitVersion("1");
	
	public static final EventBus EVENT_BUS = new EventBus();

	public static ScaledResolution SCALED_RESOLUTION;
	public static int SCREEN_WIDTH = 0, SCREEN_HEIGHT = 0;
	
	public AlexAPI() { }
}