package com.codingforcookies.alex.api.event.key;

public enum KeyEventState {
    NONE,
    PRESSED,
    DOWN,
    RELEASED;
}