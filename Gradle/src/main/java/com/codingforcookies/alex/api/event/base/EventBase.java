package com.codingforcookies.alex.api.event.base;

public interface EventBase {
	public abstract String getName();
}