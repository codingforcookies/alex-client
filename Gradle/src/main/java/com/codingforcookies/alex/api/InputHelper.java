package com.codingforcookies.alex.api;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import com.codingforcookies.alex.api.event.EventUpdate;
import com.codingforcookies.alex.api.event.base.AlexSubscribe;
import com.codingforcookies.alex.api.event.key.KeyEventState;

public class InputHelper {
	private static KeyEventState[] mouseEvents;
	private static KeyEventState[] keyboardEvents;

	static {
		mouseEvents = new KeyEventState[Mouse.getButtonCount()];
		for(int i = 0; i < Mouse.getButtonCount(); i++)
			mouseEvents[i] = KeyEventState.NONE;

		keyboardEvents = new KeyEventState[Keyboard.KEYBOARD_SIZE];
		for(int i = 0; i < Keyboard.KEYBOARD_SIZE; i++)
			keyboardEvents[i] = KeyEventState.NONE;
	}

	@AlexSubscribe
	public void update(EventUpdate event) {
		for(int i = 0; i < mouseEvents.length; i++) {
			if(Mouse.isButtonDown(i)) {
				if(mouseEvents[i] == KeyEventState.NONE)
					mouseEvents[i] = KeyEventState.PRESSED;
				else if(mouseEvents[i] == KeyEventState.PRESSED || mouseEvents[i] == KeyEventState.DOWN)
					mouseEvents[i] = KeyEventState.DOWN;
			}else{
				if(mouseEvents[i] == KeyEventState.RELEASED)
					mouseEvents[i] = KeyEventState.NONE;
				else if(mouseEvents[i] == KeyEventState.DOWN)
					mouseEvents[i] = KeyEventState.RELEASED;
			}
		}
		
		for(int i = 0; i < keyboardEvents.length; i++) {
			if(Keyboard.isKeyDown(i)) {
				if(keyboardEvents[i] == KeyEventState.NONE) {
					keyboardEvents[i] = KeyEventState.PRESSED;
					AlexAPI.EVENT_BUS.KEY_EVENTS.fireEvent(i, KeyEventState.PRESSED);
				}else if(keyboardEvents[i] == KeyEventState.PRESSED || keyboardEvents[i] == KeyEventState.DOWN) {
					keyboardEvents[i] = KeyEventState.DOWN;
					AlexAPI.EVENT_BUS.KEY_EVENTS.fireEvent(i, KeyEventState.DOWN);
				}
			}else{
				if(keyboardEvents[i] == KeyEventState.RELEASED)
					keyboardEvents[i] = KeyEventState.NONE;
				else if(keyboardEvents[i] != KeyEventState.NONE) {
					keyboardEvents[i] = KeyEventState.RELEASED;
					AlexAPI.EVENT_BUS.KEY_EVENTS.fireEvent(i, KeyEventState.RELEASED);
				}
			}
		}
	}

	public static boolean isKeyDown(int key) {
		return keyboardEvents[key] == KeyEventState.DOWN;
	}

	public static boolean isKeyPressed(int key) {
		return keyboardEvents[key] == KeyEventState.PRESSED;
	}

	public static boolean isKeyReleased(int key) {
		return keyboardEvents[key] == KeyEventState.RELEASED;
	}

	public static boolean isButtonDown(int key) {
		return mouseEvents[key] == KeyEventState.DOWN;
	}

	public static boolean isButtonPressed(int key) {
		return mouseEvents[key] == KeyEventState.PRESSED;
	}

	public static boolean isButtonReleased(int key) {
		return mouseEvents[key] == KeyEventState.RELEASED;
	}
}