package com.codingforcookies.alex.launch;

import java.io.File;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.codingforcookies.alex.api.AlexAPI;
import com.codingforcookies.alex.asm.ClassTransformer;
import com.codingforcookies.alex.bootstrap.AlexBootstrap;

import net.minecraftforge.fml.relauncher.FMLInjectionData;
import net.minecraftforge.fml.relauncher.IFMLCallHook;
import net.minecraftforge.fml.relauncher.IFMLLoadingPlugin;
import net.minecraftforge.fml.relauncher.IFMLLoadingPlugin.TransformerExclusions;

@TransformerExclusions({ "com.codingforcookies.alex" })
public class Alex implements IFMLLoadingPlugin, IFMLCallHook {
	public static Logger logger = LogManager.getLogger("AlexClient");
	
	public File minecraftDir;
	public String currentMcVersion;
	
	private static Alex alexInstance;
	public static Alex get() { return alexInstance; }
	
	private static AlexAPI api;
	public static AlexAPI api() { return api; }
	
	private AlexBootstrap bootstrap;
	public void initBootstrap() { bootstrap = new AlexBootstrap(minecraftDir); }
	public AlexBootstrap getBootstrap() { return bootstrap; }
	
	public Alex() {
		alexInstance = this;
		api = new AlexAPI();
		
		String fixed = ((File)FMLInjectionData.data()[6]).getAbsolutePath();
		if(fixed.endsWith("."))
			fixed = fixed.substring(0, fixed.length() - 1);
		minecraftDir = new File(fixed);
		currentMcVersion = (String)FMLInjectionData.data()[4];
	}
	
	public String[] getASMTransformerClass() {
		return new String[] {  ClassTransformer.class.getName() };
	}

	@Override
	public String getModContainerClass() {
		return AlexModContainer.class.getName();
	}

	@Override
	public String getSetupClass() {
		return getClass().getName();
	}

	@Override
	public void injectData(Map<String, Object> data) { }

	@Override
	public String getAccessTransformerClass() {
		return null;
	}
	
	@Override
	public Void call() throws Exception {
		return null;
	}
}