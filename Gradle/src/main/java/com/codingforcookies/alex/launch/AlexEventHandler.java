package com.codingforcookies.alex.launch;

import com.codingforcookies.alex.api.AlexAPI;
import com.codingforcookies.alex.bootstrap.gui.GuiAlexLoading;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

public class AlexEventHandler {
	Minecraft mc;
	Alex alexInstance;
	boolean loaded = false;
	
	public AlexEventHandler() {
		mc = Minecraft.getMinecraft();
		alexInstance = Alex.get();
	}
	
	@SubscribeEvent
	public void onClientTick(TickEvent.ClientTickEvent event) {
		if(!loaded) {
			if(alexInstance.getBootstrap() == null)
				return;
			if(!(mc.currentScreen instanceof GuiMainMenu))
				return;
			
			mc.displayGuiScreen(new GuiAlexLoading());
			alexInstance.getBootstrap().beginLoading();
			
			loaded = true;
		}else{
			if(AlexAPI.SCREEN_WIDTH != mc.displayWidth || AlexAPI.SCREEN_HEIGHT != mc.displayHeight) {
				AlexAPI.SCREEN_WIDTH = mc.displayWidth;
				AlexAPI.SCREEN_HEIGHT = mc.displayHeight;
				
				ScaledResolution sc = new ScaledResolution(mc);
				AlexAPI.SCALED_RESOLUTION = sc;
			}
		}
	}
}