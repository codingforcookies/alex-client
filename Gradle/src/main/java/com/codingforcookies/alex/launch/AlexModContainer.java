package com.codingforcookies.alex.launch;

import com.codingforcookies.alex.api.event.base.EventAbstract;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import net.minecraft.client.Minecraft;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.DummyModContainer;
import net.minecraftforge.fml.common.LoadController;
import net.minecraftforge.fml.common.MetadataCollection;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

/**
 * @Author Borlea
 * @Github https://github.com/borlea/
 * @Website http://codingforcookies.com/
 * @since Mar 22, 2016
 */
public class AlexModContainer extends DummyModContainer {
	public AlexModContainer() {
		super(MetadataCollection.from(MetadataCollection.class.getResourceAsStream("/axmod.info"), "Alex").getMetadataForId("Alex", null));
	}

	@Override
	public boolean registerBus(EventBus bus, LoadController controller) {
		bus.register(this);
		return true;
	}

	@Subscribe
	public void preInit(FMLPreInitializationEvent event) {
		if(event.getSide().isClient())
			EventAbstract.mc = Minecraft.getMinecraft();
	}

	@Subscribe
	public void init(FMLInitializationEvent event) {
		Alex.get().initBootstrap();
		MinecraftForge.EVENT_BUS.register(new AlexEventHandler());
	}
}