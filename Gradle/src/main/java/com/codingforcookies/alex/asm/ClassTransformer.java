package com.codingforcookies.alex.asm;

import net.minecraft.launchwrapper.IClassTransformer;

/**
 * @Author Borlea
 * @Github https://github.com/borlea/
 * @Website http://codingforcookies.com/
 * @since Mar 17, 2016
 */
public class ClassTransformer implements IClassTransformer {
	@Override
	public byte[] transform(String name, String transformedName, byte[] basicClass) {
		return basicClass;
	}
}