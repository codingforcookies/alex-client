package com.codingforcookies.alexcore.ui;

public interface UIEvent {
	void fire(UIComponent component);

	void onDrag(UIComponent component, float mouseX, float mouseY);
}