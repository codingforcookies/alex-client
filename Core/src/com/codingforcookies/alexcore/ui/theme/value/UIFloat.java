package com.codingforcookies.alexcore.ui.theme.value;

import com.codingforcookies.alexcore.ui.theme.UIValue;

public class UIFloat extends UIValue<Float> {
	public UIFloat(String value) {
		super(Float.parseFloat(value));
	}
}