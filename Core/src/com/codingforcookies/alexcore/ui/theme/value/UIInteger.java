package com.codingforcookies.alexcore.ui.theme.value;

import com.codingforcookies.alexcore.ui.theme.UIValue;

public class UIInteger extends UIValue<Integer> {
	public UIInteger(String value) {
		super(Integer.parseInt(value));
	}
}