package com.codingforcookies.alexcore.ui.theme.value;

import com.codingforcookies.alexcore.ui.theme.UIValue;

public class UIDouble extends UIValue<Double> {
	public UIDouble(String value) {
		super(Double.parseDouble(value));
	}
}