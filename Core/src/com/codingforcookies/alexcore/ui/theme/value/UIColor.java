package com.codingforcookies.alexcore.ui.theme.value;

import org.lwjgl.util.Color;

import com.codingforcookies.alexcore.ui.theme.UIValue;

public class UIColor extends UIValue<Color> {
	public UIColor(String value) {
		super(parseColor(value));
	}

	private static Color parseColor(String value) {
		if(value.startsWith("#"))
			value = value.substring(1);

		if(value.length() < 6) {
			if(value.length() == 3)
				value += value;
			else
				value = "000000";
		}

		int a = 255;

		int o = 0;
		if(value.length() == 8) {
			o += 2;
			a = Integer.valueOf(value.substring(0, 2), 16);
		}

		int r = Integer.valueOf(value.substring(0 + o, 2 + o), 16);
		int g = Integer.valueOf(value.substring(2 + o, 4 + o), 16);
		int b = Integer.valueOf(value.substring(4 + o, 6 + o), 16);

		if(r > 255)
			r = 255;
		if(g > 255)
			g = 255;
		if(b > 255)
			b = 255;
		if(a > 255)
			a = 255;

		return new Color(r, g, b, a);
	}
}