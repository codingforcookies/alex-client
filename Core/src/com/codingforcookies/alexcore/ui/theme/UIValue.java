package com.codingforcookies.alexcore.ui.theme;

public class UIValue<T> {
	protected T value;
	public T getValue() { return value; }
	
	public UIValue(T value) {
		this.value = value;
	}
}