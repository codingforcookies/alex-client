package com.codingforcookies.alexcore.ui.theme;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.codingforcookies.alex.api.util.AlexUtil;

public class ThemeManager {
	private final File themeLocation;

	private List<UITheme> availableThemes;
	public List<UITheme> getThemes() { return availableThemes; }
	
	private UITheme currentTheme;
	public UITheme getTheme() { return currentTheme; }
	public void setTheme(UITheme theme) { this.currentTheme = theme; }

	public ThemeManager() throws Exception {
		themeLocation = new File(AlexUtil.getAlexFolder(), "Themes");
		
		themeLocation.mkdirs();
		
		File file = new File(themeLocation, "default.alextheme");
		if(!file.exists())
			AlexUtil.exportResource("/default.alextheme", file);
		
		loadThemes();
	}
	
	public void loadThemes() {
		AlexUtil.log("Loading themes.");
		availableThemes = new ArrayList<>();

		for(File file : themeLocation.listFiles())
			if(file.getName().endsWith(".alextheme")) {
				UITheme theme = new UITheme(file);
				availableThemes.add(theme);
			}
		
		currentTheme = availableThemes.get(0);
	}
}