package com.codingforcookies.alexcore.ui.theme;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Field;
import java.util.HashMap;

import org.lwjgl.util.Color;

import com.codingforcookies.alexcore.ui.theme.value.UIColor;
import com.codingforcookies.alexcore.ui.theme.value.UIFloat;

/**
 * Themes are reminiscent of CSS in how they are loaded. This class holds all name, creator, description, and class information.
 * @author Stumblinbear
 */
public class UITheme {
	private static @interface UIOption { String id(); }
	
	public class UIOptions {
		@UIOption(id = "color.bg")
		protected UIColor bgColor = null;
		public Color getBackgroundColor() {
			if(bgColor == null)
				return GLOBALS.getBackgroundColor();
			return bgColor.value;
		}

		@UIOption(id = "color.fg")
		protected UIColor fgColor = null;
		public Color getForegroundColor() {
			if(fgColor == null)
				return GLOBALS.getForegroundColor();
			return fgColor.value;
		}

		@UIOption(id = "alpha")
		protected UIFloat alpha = null;
		public float getAlpha() {
			if(alpha == null)
				return GLOBALS.getAlpha();
			return alpha.value;
		}
	}

	private UIOptions GLOBALS;
	private HashMap<String, String> VARIABLES = new HashMap<>();

	{
		GLOBALS = new UIOptions();
		{
			GLOBALS.bgColor = new UIColor("#000000");
			GLOBALS.fgColor = new UIColor("#000000");
			GLOBALS.alpha = new UIFloat("0");
		}
	}
	

	private String name;
	public String getName() { return name; }
	
	private String author;
	public String getAuthor() { return author; }

	private HashMap<String, UIOptions> values = new HashMap<>();
	public UIOptions getNode(String node) {
		if(values.containsKey(node))
			return values.get(node);
		return GLOBALS;
	}

	public UITheme(File file) {
		try(BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line;
			while((line = br.readLine()) != null) {
				if(line.startsWith("name=")) {
					name = line.split("=", 2)[1];
				}else if(line.startsWith("name=")) {
					author = line.split("=", 2)[1];
				}else if(line.contains("#"))
					addNode(line);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addNode(String line) {
		String[] parse = line.split("#", 2);
		String[] value = parse[1].split("=", 2);
		
		UIOptions options = new UIOptions();
		
		if(parse[0].equals("*"))
			options = GLOBALS;
		else if(parse[0].equals("var")) {
			VARIABLES.put(value[0], value[1]);
			return;
		}else if(values.containsKey(parse[0]))
			options = values.get(parse[0]);
		
		Field f = getField(options, value[0]);
		if(f == null)
			return;
		
		String val = value[1];
		if(val.startsWith("{") && val.endsWith("}")) {
			val = val.substring(1, val.length() - 1);
			val = VARIABLES.get(val);
		}
		
		try {
			f.set(options, f.getType().getDeclaredConstructor(String.class).newInstance(val));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private Field getField(UIOptions options, String id) {
		for(Field f : options.getClass().getDeclaredFields()) {
			if(f.isAnnotationPresent(UIOption.class)) {
				UIOption anno = f.getAnnotation(UIOption.class);
				if(anno.id().equals(id))
					return f;
			}
		}
		return null;
	}
}