package com.codingforcookies.alexcore.ui;

public enum UIState {
	HOVER,
	GRABBED;
}