package com.codingforcookies.alexcore.ui;

import com.codingforcookies.alexcore.AlexCore;
import com.codingforcookies.alexcore.base.AlexAlign;
import com.codingforcookies.alexcore.base.IAlexRenderable;
import com.codingforcookies.alexcore.base.IAlexTickable;
import com.codingforcookies.alexcore.ui.theme.ThemeManager;
import com.codingforcookies.alexcore.util.Vec4f;

public abstract class AlexWidget implements IAlexRenderable, IAlexTickable {
	public boolean scheduledForDrop = false;
	protected ThemeManager themeManager = AlexCore.get().themeManager();
	
	protected IAlexRenderable parent;
	public void setParent(IAlexRenderable parent) { this.parent = parent; }
	
	private Vec4f bounds;
	@Override
	public void setBounds(float x, float y, float w, float h) { bounds = new Vec4f(x, y, w, h); }
	@Override
	public Vec4f getBounds() { return bounds; }

	protected AlexAlign anchor = AlexAlign.CENTER;
	@Override
	public void setAnchor(AlexAlign anchor) { this.anchor = anchor; }
	@Override
	public AlexAlign getAnchor() { return anchor; }
	
	protected final String themeID;
	
	public AlexWidget(String themeID) {
		this.themeID = themeID;
	}
	
	public boolean breakMouse() { return false; }
}