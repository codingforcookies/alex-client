package com.codingforcookies.alexcore.ui.render;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.Color;

import com.codingforcookies.alexcore.AlexCore;
import com.codingforcookies.alexcore.ui.theme.ThemeManager;
import com.codingforcookies.alexcore.ui.theme.UITheme.UIOptions;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;

public class RenderHelper {
	private static ThemeManager themeManager = AlexCore.get().themeManager();
	
	private static FontRenderer fontRenderer;

	private static Tessellator tessellator;
	private static VertexBuffer buffer;

	static {
		fontRenderer = Minecraft.getMinecraft().getRenderManager().getFontRenderer();

		tessellator = Tessellator.getInstance();
		buffer = tessellator.getBuffer();
	}


	public static void renderWithTheme(String themeID, float w, float h) {
		renderWithTheme(themeID, w, h, null);
	}

	/**
	 * Renders a square. But reads the theme file and appends all defined components such as borders. Includes a color override.<br><br>
	 * Warning: Does not run GL11.glPushMatrix() or GL11.glPopMatrix()
	 */
	public static void renderWithTheme(String themeID, float w, float h, Color c) {
		if(c == null) {
			UIOptions options = themeManager
					.getTheme()
					.getNode(themeID);
			c = options.getBackgroundColor();
		}

		renderBox(0, 0, w, h, packRGBA(c));
	}

	public static void renderBox(float x, float y, float w, float h, Color c) {
		renderBox(x, y, w, h, packRGBA(c));
	}

	public static void renderBox(float x, float y, float w, float h, int argb) {
		float a = ((argb >> 24) & 0xFF) / 255F;
		float r = ((argb >> 16) & 0xFF) / 255F;
		float g = ((argb >> 8) & 0xFF) / 255F;
		float b = (argb & 0xFF) / 255F;
        
		buffer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_COLOR);
		{
			buffer.pos(x, y, 0).color(r, g, b, a).endVertex();
			buffer.pos(x, y + h, 0).color(r, g, b, a).endVertex();
			buffer.pos(x + w, y + h, 0).color(r, g, b, a).endVertex();
			buffer.pos(x + w, y, 0).color(r, g, b, a).endVertex();
		}
		tessellator.draw();
	}

	public static void renderBoxTex(float x, float y, float w, float h, float texW, float texH) {
		buffer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
		{
			buffer.pos(x, y, 0).tex(0, 0).endVertex();
			buffer.pos(x, y + h, 0).tex(0, texH).endVertex();
			buffer.pos(x + w, y + h, 0).tex(texW, texH).endVertex();
			buffer.pos(x + w, y, 0).tex(texW, 0).endVertex();
		}
		tessellator.draw();
	}

	public static void drawString(String themeID, String string, float x, float y) {
		UIOptions options = themeManager.getTheme().getNode(themeID);

		fontRenderer.drawString(string, x, y, packRGBA(options.getForegroundColor()), false);
	}

	public static int packRGBA(Color c) {
		return (c.getRed() << 0) | (c.getGreen() << 8) | (c.getBlue() << 16) | (c.getAlpha() << 24);
	}
}