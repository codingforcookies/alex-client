package com.codingforcookies.alexcore.ui.component;

import com.codingforcookies.alex.api.event.EventRender;
import com.codingforcookies.alexcore.ui.UIComponent;
import com.codingforcookies.alexcore.ui.render.RenderHelper;

public class UIButton extends UIComponent {
	/**
	 * The text to be drawn.
	 */
	private String text;
	
	public UIButton(String text) {
		super("button");
		
		this.text = text;
	}
	
	@Override
	public void render(EventRender event) {
		String drawntext = text;
		
		int stringWidth = event.fontRenderer.getStringWidth(drawntext);
		
		if(stringWidth > getBounds().getW()) {
			drawntext += "...";
			while((stringWidth = event.fontRenderer.getStringWidth(drawntext)) > getBounds().getW())
				drawntext = drawntext.substring(0, drawntext.length() - 4) + "...";
		}
		
		getBounds().pushLocation();
		{
			RenderHelper.renderWithTheme(themeID, getBounds().getW(), getBounds().getH());
			
			RenderHelper.drawString(themeID, drawntext, getBounds().getW() / 2 - stringWidth / 2, 0);
		}
		getBounds().popLocation();
	}
	
	/**
	 * Get the text to be drawn.
	 */
	public String getText() {
		return text;
	}
	
	/**
	 * Set the text to be drawn.
	 */
	public UIButton setText(String text) {
		this.text = text;
		return this;
	}
}