package com.codingforcookies.alexcore.ui.component;

import com.codingforcookies.alex.api.event.EventRender;
import com.codingforcookies.alexcore.base.AlexAlign;
import com.codingforcookies.alexcore.ui.UIComponent;
import com.codingforcookies.alexcore.ui.render.RenderHelper;

public class UILabel extends UIComponent {
	/**
	 * The text to be drawn.
	 */
	private String text;
	
	public UILabel(String text) {
		super("label");
		
		this.text = text;
	}
	
	@Override
	public void render(EventRender event) {
		String drawntext = text;
		
		int stringWidth = event.fontRenderer.getStringWidth(drawntext);
		
		if(stringWidth > getBounds().getW()) {
			drawntext += "...";
			while((stringWidth = event.fontRenderer.getStringWidth(drawntext)) > getBounds().getW())
				drawntext = drawntext.substring(0, drawntext.length() - 4) + "...";
		}
		
		getBounds().pushLocation();
		{
			RenderHelper.drawString(themeID, drawntext, (align == AlexAlign.CENTER ? getBounds().getW() / 2 - event.fontRenderer.getStringWidth(drawntext) / 2 : align == AlexAlign.RIGHT ? getBounds().getW() - event.fontRenderer.getStringWidth(drawntext) : 0), 0);
		}
		getBounds().popLocation();
	}
	
	/**
	 * Get the text to be drawn.
	 */
	public String getText() {
		return text;
	}
	
	/**
	 * Set the text to be drawn.
	 */
	public UILabel setText(String text) {
		this.text = text;
		return this;
	}
}