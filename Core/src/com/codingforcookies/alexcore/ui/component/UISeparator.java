package com.codingforcookies.alexcore.ui.component;

import com.codingforcookies.alex.api.event.EventRender;
import com.codingforcookies.alexcore.ui.UIComponent;
import com.codingforcookies.alexcore.ui.render.RenderHelper;

public class UISeparator extends UIComponent {
	public UISeparator(String text) {
		super("separator");
	}
	
	@Override
	public void render(EventRender event) {
		getBounds().pushLocation();
		{
			if(getBounds().getW() >= getBounds().getH())
				RenderHelper.renderWithTheme(themeID, getBounds().getW(), 1);
			else
				RenderHelper.renderWithTheme(themeID, 1, getBounds().getH());
		}
		getBounds().popLocation();
	}
}