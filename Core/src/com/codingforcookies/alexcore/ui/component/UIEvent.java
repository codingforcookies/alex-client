package com.codingforcookies.alexcore.ui.component;

public interface UIEvent {
	public abstract void onEvent();
}