package com.codingforcookies.alexcore.ui.component;

import com.codingforcookies.alex.api.event.EventRender;
import com.codingforcookies.alexcore.ui.UIComponent;
import com.codingforcookies.alexcore.ui.render.RenderHelper;

public class UIBox extends UIComponent {
	public UIBox(String text) {
		super("box");
	}
	
	@Override
	public void render(EventRender event) {
		getBounds().pushLocation();
		{
			RenderHelper.renderWithTheme(themeID, getBounds().getW(), getBounds().getH());
		}
		getBounds().popLocation();
	}
}