package com.codingforcookies.alexcore.ui.component;

import com.codingforcookies.alex.api.event.EventRender;
import com.codingforcookies.alexcore.ui.UIComponent;
import com.codingforcookies.alexcore.ui.render.RenderHelper;
import com.codingforcookies.alexcore.ui.theme.UITheme.UIOptions;

public class UIProgressBar extends UIComponent {
	private float progress = 0F;
	
	public UIProgressBar(String text) {
		super("progressbar");
	}
	
	@Override
	public void render(EventRender event) {
		getBounds().pushLocation();
		{
			RenderHelper.renderWithTheme(themeID, getBounds().getW(), getBounds().getH());
			
			UIOptions options = themeManager.getTheme().getNode(themeID);
			RenderHelper.renderWithTheme(themeID, getBounds().getW() * progress, getBounds().getH(), options.getForegroundColor());
		}
		getBounds().popLocation();
	}
	
	/**
	 * Set the current progress. 0F = 0% .5F = 50% 1F = 100%
	 * @param progress
	 */
	public void setProgress(float progress) {
		this.progress = progress;
	}
	
	/**
	 * Gets the current progress. 0F = 0% .5F = 50% 1F = 100%
	 */
	public float getProgress() {
		return progress;
	}
}