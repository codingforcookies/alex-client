package com.codingforcookies.alexcore.ui;

import com.codingforcookies.alex.api.event.EventRender;
import com.codingforcookies.alex.api.event.EventUpdate;
import com.codingforcookies.alexcore.base.AlexAlign;

/**
 * Common class for all Maya UI Components.
 * @author Stumblinbear
 */
public class UIComponent extends AlexWidget {
	protected int fontsize = 2;
	public void setSmall() { this.fontsize = 1; }
	public void setLarge(float size) { this.fontsize = 3; }

	protected AlexAlign align;
	public void setAlign(AlexAlign align) { this.align = align; }
	public AlexAlign getAlign() { return align; }

	protected boolean[] state = new boolean[UIState.values().length];
	{
		for(int i = 0; i < UIState.values().length; i++)
			state[i] = false;
	}
	
	public boolean getState(UIState state) {
		return this.state[state.ordinal()];
	}
	
	public void setState(UIState state, boolean b) {
		this.state[state.ordinal()] = b;
	}
	
	public UIEvent event;
	
	public UIComponent(String themeID) {
		super(themeID);
	}

	@Override
	public void render(EventRender event) { }

	@Override
	public void update(EventUpdate event) { }
}