package com.codingforcookies.alexcore.window.nexus;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import com.codingforcookies.alex.api.AlexAPI;
import com.codingforcookies.alex.api.event.EventRender;
import com.codingforcookies.alex.api.event.EventUpdate;
import com.codingforcookies.alexcore.AlexCore;
import com.codingforcookies.alexcore.ui.AlexWidget;
import com.codingforcookies.alexcore.ui.render.RenderHelper;
import com.codingforcookies.alexcore.ui.theme.UITheme.UIOptions;
import com.codingforcookies.alexcore.widget.WidgetWindow;

import net.minecraft.client.renderer.GlStateManager;

public class WindowNexus extends WidgetWindow {
	public static boolean isOpen() {
		for(AlexWidget window : AlexCore.get().widgetManager().getWidgets())
			if(window instanceof WindowNexus)
				return true;
		return false;
	}
	
	public static void close() {
		for(AlexWidget window : AlexCore.get().widgetManager().getWidgets())
			if(window instanceof WindowNexus) {
				AlexCore.get().widgetManager().removeWidget(window);
				return;
			}
	}
	
	float animtime;
	
	float offset;
	final float iconSize = 32F;
	
	public WindowNexus() {
		super("nexus", 0F, 0F, AlexAPI.SCREEN_WIDTH, 46F);
		
		animtime = getBounds().getH() / 2;
	}

	@Override
	public void init() { }

	@Override
	public void onClose() { }
	
	@Override
	public void update(EventUpdate event) {
		offset = AlexAPI.SCALED_RESOLUTION.getScaledWidth() / 2F;
		offset -= Nexus.nexusMenu.size() * iconSize / 2F;
		offset -= (Nexus.nexusMenu.size() - 1) * iconSize / 2F;
		if(animtime > 0F)
			animtime -= 1F;
		else
			animtime = 0F;
		for(int i = 0; i < Nexus.nexusMenu.size(); i++){
			NexusMenu menu = Nexus.nexusMenu.get(i);
			float mouseX = Mouse.getX();
			float mouseY = Mouse.getY();
			float minX = offset + (i * iconSize) * 2;
			float maxX = offset + (i * iconSize) * 2 + iconSize;
			
			if(mouseX >= minX && mouseX <= maxX && AlexAPI.SCREEN_HEIGHT - mouseY < getBounds().getH()){
				menu.hover = true;
				if(Mouse.isButtonDown(0))
				    menu.clicked = true;
			}else{
				menu.hover = false;
			}
			menu.update(event);
		}
	}

	@Override
	public void render(EventRender event) {
		getBounds().pushLocation();
		GL11.glTranslatef(0, -animtime, 0F);

		GlStateManager.enableBlend();
		
		UIOptions o = themeManager.getTheme().getNode("nexus");
		RenderHelper.renderWithTheme(themeID, getBounds().getW(), getBounds().getH(), o.getBackgroundColor());

		/*GL11.glPushMatrix();
		{
			GL11.glTranslatef(offset, 0F, 0F);
			RenderHelper.renderBox(0, 0, iconSize, iconSize, 0xFFFFFFFF);
		}
		GL11.glPopMatrix();*/

		GL11.glTranslatef(offset, 0F, 0F);
		for(int i = 0; i < Nexus.nexusMenu.size(); i++) {
			// RenderHelper.renderBox(getBounds().getX(), 0, iconSize, iconSize, 0x999FFFFF);
			Nexus.nexusMenu.get(i).render(event, getBounds().getX(), iconSize, i, o.getForegroundColor());
			GL11.glTranslatef(iconSize * 2, 0F, 0F);
		}
	}
}