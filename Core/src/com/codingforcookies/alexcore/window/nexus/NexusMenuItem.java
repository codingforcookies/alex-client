package com.codingforcookies.alexcore.window.nexus;

public class NexusMenuItem {
	public String text;
	public Runnable run;
	
	public NexusMenuItem(String text, Runnable run) {
		this.text = text;
		this.run = run;
	}
}