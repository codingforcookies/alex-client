package com.codingforcookies.alexcore.window.nexus;

import java.util.ArrayList;
import java.util.List;

import com.codingforcookies.alexcore.texture.AlexTexture;

public class Nexus {
	protected static List<NexusMenu> nexusMenu = new ArrayList<NexusMenu>();
	
	public static NexusMenu registerMenu(AlexTexture tex, String text) {
		NexusMenu menu = new NexusMenu(tex, text);
		nexusMenu.add(menu);
		return menu;
	}
}