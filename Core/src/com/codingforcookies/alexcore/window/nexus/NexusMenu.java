package com.codingforcookies.alexcore.window.nexus;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Mouse;
import org.lwjgl.util.Color;

import com.codingforcookies.alex.api.AlexAPI;
import com.codingforcookies.alex.api.event.EventRender;
import com.codingforcookies.alex.api.event.EventUpdate;
import com.codingforcookies.alexcore.texture.AlexTexture;
import com.codingforcookies.alexcore.ui.render.RenderHelper;

import net.minecraft.client.renderer.GlStateManager;

public class NexusMenu {
	public List<NexusMenuItem> menuItems = new ArrayList<>();
	
	public boolean hover = false;
	public int hovered = -1;
	
	public boolean clicked = false;
	public float animtime = 0F;
	
	private AlexTexture tex;
	private String text;
	
	public NexusMenu(AlexTexture tex, String text) {
		this.tex = tex;
		this.text = text;
	}
	
	public void addItem(String text, Runnable run) {
		menuItems.add(new NexusMenuItem(text, run));
	}
	
	public void update(EventUpdate event) {
		if(!hover && Mouse.isButtonDown(0)) {
			clicked = false;
			animtime = 0F;
			
			if(hovered != -1) {
				if(menuItems.get(hovered).run != null)
					menuItems.get(hovered).run.run();
				
				hovered = -1;
				
				WindowNexus.close();
			}
		}else
			animtime += .04F;
	}
	
	public void render(EventRender event, float xOffset, float iconSize, int index, Color c) {
		int cc = 0xFFFFFFFF;
		GlStateManager.pushMatrix();
		{
			if(clicked) {
				float offsetX = -20F;
				float offsetY = -25F;
				
				hovered = -1;
				
				for(int i = 0; i < (animtime > menuItems.size() ? menuItems.size() : (int)animtime); i++) {
					float animoffset = (animtime - i + 1F) * 2F - 10F;
					if(animoffset > 10F)
						animoffset = 10F;
					
					float mouseXOffset = xOffset + offsetX + index * 41F;
					float mouseY = AlexAPI.SCREEN_HEIGHT - Mouse.getY();
					
					if(Mouse.getX() > mouseXOffset - animoffset && Mouse.getX() < mouseXOffset + 100F + animoffset &&
							mouseY > Math.abs(offsetY) + 40F && mouseY < Math.abs(offsetY) + 60F)
						hovered = i;
					
					RenderHelper.renderBox(offsetX - animoffset, offsetY, animoffset + 100F, 20F, cc);
					
					event.fontRenderer.drawString(menuItems.get(i).text, 30F - event.fontRenderer.getStringWidth(menuItems.get(i).text) / 2, offsetY + 15F, cc, false);
					
					offsetY -= 20F;
				}
			}
			
			GlStateManager.enableTexture2D();
			
			tex.bind();
			
			RenderHelper.renderBoxTex(0F, 0F, 30F, 30F, 1F, 1F);
			event.fontRenderer.drawString(text, (30F - event.fontRenderer.getStringWidth(text)) / 2, 35F, cc, false);
			GlStateManager.disableTexture2D();
		}
		GlStateManager.popMatrix();
	}
}