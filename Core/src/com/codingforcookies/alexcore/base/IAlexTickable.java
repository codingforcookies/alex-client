package com.codingforcookies.alexcore.base;

import com.codingforcookies.alex.api.event.EventUpdate;

public interface IAlexTickable {
	void update(EventUpdate event);
}