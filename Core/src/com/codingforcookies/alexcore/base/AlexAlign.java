package com.codingforcookies.alexcore.base;

public enum AlexAlign {
	TOP(1, 0, 0, 0),
	RIGHT(0, 1, 0, 0),
	BOTTOM(0, 0, 1, 0),
	LEFT(0, 0, 0, 1),
	CENTER(0, 0, 0, 0),
	VERTICAL(1, 0, 1, 0),
	HORIZONTAL(0, 1, 0, 1),
	
	TOPLEFT(1, 0, 0, 1),
	TOPRIGHT(1, 1, 0, 0),
	BOTTOMLEFT(0, 0, 1, 1),
	BOTTOMRIGHT(0, 1, 1, 0);
	
	/**
	 * Connections. Dictates which sides should stick.
	 */
	public boolean top, right, bottom, left;
	
	AlexAlign(int top, int right, int bottom, int left) {
		this.top = top == 0 ? false : true;
		this.right = right == 0 ? false : true;
		this.bottom = bottom == 0 ? false : true;
		this.left = left == 0 ? false : true;
	}
}