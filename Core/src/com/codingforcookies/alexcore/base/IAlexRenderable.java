package com.codingforcookies.alexcore.base;

import com.codingforcookies.alex.api.event.EventRender;
import com.codingforcookies.alexcore.util.Vec4f;

public interface IAlexRenderable {
	void setBounds(float x, float y, float w, float h);
	Vec4f getBounds();
	
	void setAnchor(AlexAlign anchor);
	AlexAlign getAnchor();
	
	void render(EventRender event);
}