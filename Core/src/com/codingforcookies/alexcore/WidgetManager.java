package com.codingforcookies.alexcore;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import com.codingforcookies.alex.api.AlexAPI;
import com.codingforcookies.alex.api.IModule;
import com.codingforcookies.alex.api.event.EventRender;
import com.codingforcookies.alex.api.event.EventUpdate;
import com.codingforcookies.alex.api.event.base.AlexSubscribe;
import com.codingforcookies.alexcore.ui.AlexWidget;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;

public class WidgetManager {
	private Set<AlexWidget> pendingRemoval = new HashSet<>();
	private Set<AlexWidget> widgets = new LinkedHashSet<>();
	public Set<AlexWidget> getWidgets() { return widgets; }
	
	public void addWidget(AlexWidget widget) { widgets.add(widget); }
	public void removeWidget(AlexWidget widget) { pendingRemoval.add(widget); }
	
	public WidgetManager(IModule module) {
		AlexAPI.EVENT_BUS.register(module, this);
	}
	
	@AlexSubscribe
	public void onRender(EventRender event) {
		GlStateManager.disableTexture2D();
		for(AlexWidget widget : widgets) {
			GlStateManager.pushMatrix();
			{
				widget.render(event);
			}
			GlStateManager.popMatrix();
		}
		GlStateManager.enableTexture2D();
	}
	
	@AlexSubscribe
	public void onTick(EventUpdate event) {
		for(AlexWidget widget : pendingRemoval)
			widgets.remove(widget);
		
		boolean breakMouse = false;
		
		for(AlexWidget widget : widgets) {
			widget.update(event);
			
			if(widget.breakMouse())
				breakMouse = true;
		}
		
		GuiScreen screen = event.getMinecraft().currentScreen;
		
		if(screen == null) {
			if(breakMouse)
				event.getMinecraft().displayGuiScreen(new WidgetScreen());
		}else if(!breakMouse && screen instanceof WidgetScreen)
			event.getMinecraft().displayGuiScreen(null);
	}
}

class WidgetScreen extends GuiScreen { }