package com.codingforcookies.alexcore.texture;

import net.minecraft.client.renderer.GlStateManager;

/**
 * Class for a loaded texture
 * @author Stumblinbear
 */
public class AlexTexture {
	private int texture = 0;
	
	public AlexTexture(int textureid) {
		texture = textureid;
	}
	
	/**
	 * Bind the texture
	 */
	public void bind() {
		GlStateManager.bindTexture(texture);
	}
}