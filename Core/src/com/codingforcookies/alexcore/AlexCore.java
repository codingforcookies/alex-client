package com.codingforcookies.alexcore;

import org.lwjgl.input.Keyboard;

import com.codingforcookies.alex.api.AlexAPI;
import com.codingforcookies.alex.api.IModule;
import com.codingforcookies.alex.api.event.EventUpdate;
import com.codingforcookies.alex.api.event.base.AlexSubscribe;
import com.codingforcookies.alex.api.event.key.KeyListener;
import com.codingforcookies.alex.api.util.AlexUtil;
import com.codingforcookies.alexcore.texture.AlexTextureLoader;
import com.codingforcookies.alexcore.ui.theme.ThemeManager;
import com.codingforcookies.alexcore.window.nexus.Nexus;
import com.codingforcookies.alexcore.window.nexus.NexusMenu;
import com.codingforcookies.alexcore.window.nexus.WindowNexus;

public class AlexCore implements IModule {
	public static final String VERSION = "Chill Leopard";
	
	private static AlexCore instance;
	public static AlexCore get() { return instance; }
	
	private ThemeManager themeManager;
	public ThemeManager themeManager() { return themeManager; }
	
	private WidgetManager widgetManager;
	public WidgetManager widgetManager() { return widgetManager; }
	
	public AlexCore() {
		instance = this;
	}
	
	@Override
	public void onLoad() {
		AlexUtil.log("Alex Core v." + VERSION + ".");

		try {
			themeManager = new ThemeManager();
		} catch (Exception e) {
			e.printStackTrace();
			AlexUtil.log("Failed to load theme manager. This will cause severe errors.");
			System.exit(0);
		}
		
		widgetManager = new WidgetManager(this);
		
		AlexAPI.EVENT_BUS.register(this, this);
		
		AlexTextureLoader.loadFile("nexus-settings", AlexCore.class.getResourceAsStream("/icons/settings.png"), new Runnable() {
			public void run() {
				NexusMenu menu = Nexus.registerMenu(AlexTextureLoader.getTexture("nexus-settings"), "Settings");
				menu.addItem("Themes", new Runnable() {
					public void run() {
						
					}
				});
				menu.addItem("Basic Settings", new Runnable() {
					public void run() {
						
					}
				});
			}
		});
		
		AlexTextureLoader.loadFile("nexus-modules", AlexCore.class.getResourceAsStream("/icons/modules.png"), new Runnable() {
			public void run() {
				NexusMenu menu = Nexus.registerMenu(AlexTextureLoader.getTexture("nexus-modules"), "Modules");
				menu.addItem("Installed", new Runnable() {
					public void run() {
						
					}
				});
				menu.addItem("Axiom", new Runnable() {
					public void run() {
						
					}
				});
			}
		});
		AlexTextureLoader.loadFile("nexus-test", AlexCore.class.getResourceAsStream("/icons/modules.png"), new Runnable() {
			public void run() {
				Nexus.registerMenu(AlexTextureLoader.getTexture("nexus-test"), "Test");
			}
		});
		AlexTextureLoader.loadFile("nexus-tools", AlexCore.class.getResourceAsStream("/icons/tools.png"), new Runnable() {
			public void run() {
				Nexus.registerMenu(AlexTextureLoader.getTexture("nexus-tools"), "Tools");
			}
		});
	}
	
	@AlexSubscribe
	public void onTick(EventUpdate event) {
		AlexTextureLoader.loadQueuedTexture();
	}

	@Override
	public void onUnload() { }
	
	
	@KeyListener(key = Keyboard.KEY_GRAVE)
	public static void keyNexus() {
		if(WindowNexus.isOpen())
			WindowNexus.close();
		else
			get().widgetManager().addWidget(new WindowNexus());
	}
}