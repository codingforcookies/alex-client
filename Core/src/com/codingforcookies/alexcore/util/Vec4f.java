package com.codingforcookies.alexcore.util;

import net.minecraft.client.renderer.GlStateManager;

public class Vec4f {
    private float x;
    private float y;
    private float w;
    private float h;

    public Vec4f(float xIn, float yIn, float wIn, float hIn) {
        this.x = xIn;
        this.y = yIn;
        this.w = wIn;
        this.h = hIn;
    }

    public Vec4f(Vec4f vec) {
        this.x = vec.x;
        this.y = vec.y;
        this.w = vec.w;
        this.h = vec.h;
    }
    
    /**
     * GL Translate to the x and y.
     */
    public void pushLocation() {
    	GlStateManager.translate(x, y, 0F);
    }

    /**
     * GL Translate to the -x and -y.
     */
    public void popLocation() {
    	GlStateManager.translate(-x, -y, 0F);
    }

    public float getX() {
        return this.x;
    }

    public float getY() {
        return this.y;
    }

    public float getW() {
        return this.w;
    }

    public float getH() {
        return this.h;
    }

    public boolean equals(Object p_equals_1_) {
        if(this == p_equals_1_)
            return true;
        else if(!(p_equals_1_ instanceof Vec4f))
            return false;
        else{
            Vec4f vec4f = (Vec4f)p_equals_1_;
            return this.x != vec4f.x ? false : (this.w != vec4f.w ? false : (this.y != vec4f.y ? false : this.h == vec4f.h));
        }
    }

    public int hashCode() {
        float i = this.x;
        i = 31 * i + this.y;
        i = 31 * i + this.w;
        i = 31 * i + this.h;
        return (int)i;
    }
}