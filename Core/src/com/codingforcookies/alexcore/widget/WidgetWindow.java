package com.codingforcookies.alexcore.widget;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Mouse;

import com.codingforcookies.alex.api.event.EventRender;
import com.codingforcookies.alex.api.event.EventUpdate;
import com.codingforcookies.alexcore.ui.AlexWidget;
import com.codingforcookies.alexcore.ui.UIComponent;
import com.codingforcookies.alexcore.ui.UIState;

public abstract class WidgetWindow extends AlexWidget {
	protected List<UIComponent> components;
	public List<UIComponent> getComponents() { return components; }
	public void addComponent(UIComponent component) { component.setParent(this); components.add(component); }
	
	public WidgetWindow(String themeID, float x, float y, float w, float h) {
		super(themeID);
		
		setBounds(x, y, w, h);
		
		components = new ArrayList<UIComponent>();
	}
	
	@Override
	public boolean breakMouse() { return true; }
	
	public abstract void init();
	
	public abstract void onClose();

	@Override
	public void update(EventUpdate event) {
		for(int i = 0; i < components.size(); i++) {
			UIComponent component = components.get(i);
			
			float mouseX = Mouse.getX() - getBounds().getX();
			float mouseY = getBounds().getY() - Mouse.getY();
			
			if(mouseX > 0 && mouseX < getBounds().getW() &&
					mouseY > 0 && mouseY < getBounds().getH()) {
				float componentX = component.getBounds().getX();
				float componentY = -component.getBounds().getY();
				
				if(mouseX > componentX && mouseX < componentX + component.getBounds().getW() &&
						mouseY > componentY && mouseY < componentY + component.getBounds().getH()) {
					component.setState(UIState.HOVER, true);
					component.event.fire(component);
					
					if(Mouse.isButtonDown(0)) {
						if(!component.getState(UIState.GRABBED)) {
							component.setState(UIState.GRABBED, false);
							component.event.fire(component);
						}
					}
				}else if(component.getState(UIState.HOVER)) {
					component.setState(UIState.HOVER, false);
					component.event.fire(component);
				}
				
				if(component.getState(UIState.GRABBED)) {
					if(!Mouse.isButtonDown(0))
						component.setState(UIState.GRABBED, false);
					else
						component.event.onDrag(component, mouseX, mouseY);
				}
			}else
				component.setState(UIState.HOVER, false);
			
			component.update(event);
			if(component.scheduledForDrop) {
				components.remove(i);
				i--;
			}
		}
	}
	
	/**
	 * Renders all components in the window.
	 */
	@Override
	public void render(EventRender event) {
		getBounds().pushLocation();
		{
			drawComponents(event);
		}
		getBounds().popLocation();
	}

	protected void drawComponents(EventRender event) {
		for(UIComponent component : components)
			component.render(event);
	}
}